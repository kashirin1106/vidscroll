<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Landingpage'], function () {

    Route::get('landingpage','LandingpageController@edit')->name('landingpage');

    Route::post('landingpage/update','LandingpageController@update')->name('landingpage.update');

    Route::post('landingpage/create','LandingpageController@stores')->name('landingpage.create');

});
