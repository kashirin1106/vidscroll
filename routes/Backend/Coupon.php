<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::group(['namespace' => 'Coupon'], function () {
	
	Route::get('coupon', 'CouponController@index')->name('coupon');
	Route::post('coupon/create','CouponController@create')->name('coupon.create');
	Route::get('coupon/delete/{id}','CouponController@delete')->name('coupon.delete');
});

