<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'UserVideoManager'], function () {

    Route::get('videomanager','VideoController@index')->name('videomanager');
    Route::get('videomanager/edit/{id}','VideoController@edit')->name('videomanager.edit');
    Route::post('videomanager/get', 'VideoController@get')->name('videomanager.get');
    // Route::post('videomanager/delete/{id}','VideoController@delete')->name('videomanager.delete');

    Route::post('videomanager/update/{id}','VideoController@update')->name('videomanager.update');

    Route::any('videomanager/delete/{id}','VideoController@delete')->name('videomanager.delete');
    
    Route::get('videomanager/add','VideoController@create')->name('videomanager.add');
    Route::post('videomanager/create','VideoController@stores')->name('videomanager.create');

});
