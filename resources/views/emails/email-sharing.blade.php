@extends('emails.layouts.app')

@section('content')
<div class="content">
    <td align="left">
        <table border="0" width="80%" align="center" cellpadding="0" cellspacing="0" class="container590">
            <tr>
                <td align="left" style="color: #888888; width:20px; font-size: 16px; line-height: 24px;">
                    <!-- section text ======-->

                    <p style="line-height: 24px; margin-bottom:15px;">
                        Hello!. {{ $user_name }} shared you his video.
                    </p>
                    <p style="line-height: 24px; margin-bottom:20px;">
                        Video Title : {{ $video_data->video_title }}
                    </p>
                    <p style="line-height: 24px; margin-bottom:20px;">
                        Video Url : <a href="{{ $video_data->video_url }}" style="color: #000000; text-decoration: none;">{{ $video_data->video_url }}</a> 
                    </p>
                    <p style="line-height: 24px; margin-bottom:20px;">
                        Webpage Url : <a href="{{ $video_data->web_page_url }}" style="color: #000000; text-decoration: none;">{{ $video_data->web_page_url }}</a> 
                    </p>
                    <p style="line-height: 24px; margin-bottom:20px;">
                        Video Notes : {{ $video_data->video_notes }}
                    </p>
                    <h3>Screenshots</h3>
                    <table border="0" align="center" width="180" cellpadding="0" cellspacing="0" bgcolor="5caad2" style="margin-bottom:20px; background: #899bd7;color: white;   border-radius: 5px;">

                        <tr>
                            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                        </tr>
                        @if($screenshot_data)
                        @foreach($screenshot_data as $screenshot_list)
                        <tr>
                            <td style="width: 100px;"><img src="http://vidscroll.vidscrollapp.com/img/screenshot/thumbnail/{{ $screenshot_list->screenshot_url }}"></td>
                            <td style="width: 100px;">{{ $screenshot_list->timeframe }}</td>
                            <td class="text-center">{{ $screenshot_list->note }}</td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                        </tr>

                    </table>

                    <p style="line-height: 24px; margin-bottom:20px;">
                        Thank you for using our application!
                    </p>

                    <p style="line-height: 24px">
                        Regards,</br>
                        @yield('title', app_name())
                    </p>

                    <br/>

                    <p class="small" style="line-height: 24px; margin-bottom:20px;">
                            <!-- If you’re having trouble clicking the "Confirm Account" button, copy and paste the URL below into your web browser:  -->
                    </p>

                    <p class="small" style="line-height: 24px; margin-bottom:20px;">
                        <a href=" " target="_blank" class="lap">
                            
                        </a>
                    </p>

                    @include('emails.layouts.footer')
                </td>
            </tr>
        </table>
    </td>
</div>
@endsection
                        