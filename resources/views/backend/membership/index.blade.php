@extends ('backend.layouts.app')

@section ('title', "membership")

@section('page-header')
    <h1>Membership</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Membership</h3>

            <div class="box-tools pull-right">
                <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.membership.add')}}">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-plus"></i></span>Add New
                </a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="membership_packages_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.pages.table.title') }}</th>
                            <th>Price</th>
                            <th>Interval</th>
                            <th>Enable_save_title</th>
                            <th>Enable_save_notes</th>
                            <th>Enable_video_count</th>
                            <th>Enable_screenshot_count</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($membership_packages))
                        @foreach($membership_packages as $membership_packages_data)
                        <tr class="text-center">
                            <td>{{$membership_packages_data->name}}</td>
                            <td>{{$membership_packages_data->price}}</td>
                            <td>{{$membership_packages_data->type}}</td>
                            <td>
                                <div class="form-group">
                                    <div class="col-lg-10">
                                        <div class="control-group">
                                            <label class="control control--checkbox">
                                                {{ Form::checkbox('enable_save_title', 1, ($membership_packages_data->enable_save_title == 1) ? true : false ) }}
                                                <div class="control__indicator"></div>
                                            </label>
                                        </div>
                                    </div><!--col-lg-3-->
                                </div><!--form control-->
                            </td>
                            <td>
                                <div class="form-group">

                                    <div class="col-lg-10">
                                        <div class="control-group">
                                            <label class="control control--checkbox">
                                                {{ Form::checkbox('enable_save_notes', 1, ($membership_packages_data->enable_save_notes == 1) ? true : false ) }}
                                                <div class="control__indicator"></div>
                                            </label>
                                        </div>
                                    </div><!--col-lg-3-->
                                </div><!--form control-->
                            </td>
                            <td>
                             {{$membership_packages_data->enable_video_counts}}
                            </td>
                            <td>
                             {{$membership_packages_data->enable_screenshot_counts}}
                            </td>
                            <td>
                              <div class="btn-group action-btn">
                                <a href="{{route('admin.membership.edit',$membership_packages_data->id)}}" class="btn btn-flat btn-default">
                                    <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-pencil" data-original-title="Edit"></i>
                                </a>
                                                 
                                <a class="btn btn-flat btn-default" data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure you want to do this?" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
                            
                                <form action="{{route('admin.membership.delete',$membership_packages_data->id)}}" method="POST" name="delete_item" style="display:none">
                                    {{csrf_field()}}
                                   <input type="hidden" name="_method" value="delete">
                                    
                                </form>
                                </a>
                             </div>
                        </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('CMSpage') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        $(function() {
            var dataTable = $('#membership_packages_table').dataTable({
                 
            });

        });
       $(":checkbox").on('click',function(){
        return false;
       })
    </script>
@endsection