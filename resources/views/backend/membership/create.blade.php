@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.pages.management') . ' | ' . trans('labels.backend.pages.create'))

@section('page-header')
    <h1>
        Membership Package Manage
        <small>{{ trans('labels.backend.pages.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.membership.create', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}

    <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create Page</h3>
                <div class="box-tools pull-right">
                     <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.membership')}}">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>back
                     </a>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('title', trans('validation.attributes.backend.pages.title'), ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.title'), 'required' => 'required']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('price', "Price", ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        {{ Form::number('price', null, ['class' => 'form-control box-size', 'placeholder' => 'price', 'required' => 'required']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <div class="form-group">
                    {{Form::label('title','Interval',['class'=>'col-lg-2 control-label required'])}}
                    <div class="col-lg-10">
                        <select class="form-control box-size col-lg-10" name="type" required>
                            <option value=""></option>
                            <!-- <option value="FREE">FREE</option> -->
                            <option value="MONTH">MONTH</option>
                            <option value="YEAR">YEAR</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('enable_save_title', "Enable Save title", ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <div class="control-group">
                            <label class="control control--checkbox">
                            {{ Form::checkbox('enable_save_title', '1', true) }}
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div><!--col-lg-3-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('enable_save_notes', "Enable Save Note", ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <div class="control-group">
                            <label class="control control--checkbox">
                                {{ Form::checkbox('enable_save_notes', '1', true) }}
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div><!--col-lg-3-->
                </div><!--form control-->
                <div class="form-group">
                    {{ Form::label('enable_video_counts', "Enable Video Count", ['class' => 'col-lg-2 control-label required']) }}

                    <div class="col-lg-10">
                        <input type="text" name="enable_video_counts" class ="form-control box-size" placeholder="enable_video_counts" required>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                 <div class="form-group">
                    {{ Form::label('enable_screenshot_counts', "Enable Screenshot Count", ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                    {{ Form::text('enable_screenshot_counts', null, ['class' => 'form-control box-size', 'placeholder' => 'enable_screenshot_counts', 'required' => 'required']) }}
                    </div><!--col-lg-3-->
                </div><!--form control-->
                <div class="edit-form-btn">
                    {{ link_to_route('admin.membership', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
    @endsection
@section("after-scripts")
    <script type="text/javascript">
        Backend.Pages.init();
    </script>
@endsection
