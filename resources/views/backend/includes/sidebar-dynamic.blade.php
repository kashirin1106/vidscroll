<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }} hidden">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/membership*')) }}">
                <a href="{{ route('admin.membership') }}">
                    <i class="fa fa-gift"></i>
                    <span>Membership Package</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/user_membership*')) }}">
                <a href="{{ route('admin.user_membership') }}">
                    <i class="fa fa-user-plus"></i>
                    <span>User Membership</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/videomanager*')) }}">
                <a href="{{ route('admin.videomanager') }}">
                    <i class="fa fa-video-camera"></i>
                    <span>User Video Manager</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/screenshot*')) }}">
                <a href="{{ route('admin.screenshot') }}">
                    <i class="fa fa-image"></i>
                    <span>Screenshot</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/landingpage*')) }}">
                <a href="{{ route('admin.landingpage') }}">
                    <i class="fa fa-rss"></i>
                    <span>Landing Page</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/membershippage*')) }}">
                <a href="{{ route('admin.membershippage') }}">
                    <i class="fa fa-rss"></i>
                    <span>Membership Page</span>
                </a>
            </li>
             <li class="{{ active_class(Active::checkUriPattern('admin/coupon*')) }}">
                <a href="{{ route('admin.coupon') }}">
                    <i class="fa fa-rss"></i>
                    <span>Coupon Manage</span>
                </a>
            </li>
            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>
            {{ renderMenuItems(getMenuItems()) }}
            
            
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>