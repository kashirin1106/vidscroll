@extends ('backend.layouts.app')

@section ('title', "User Video Manage")

@section('page-header')
    <h1>User Video Manage</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">User Video Manage</h3>

            <div class="box-tools pull-right">
                <a type="button" class="btn btn-labeled btn-default" href="{{route('admin.videomanager.add')}}">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-plus"></i></span>Add New
                </a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="videomanager_table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Video Title</th>
                            <th>Video Url</th>
                            <th>Video Notes</th>
                            <th>Website Url</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($video))
                        @foreach($video as $video_data)
                        <tr>
                            <td>{{\App\Models\Membership\Usermembership::get_username($video_data->user_id)}}</td>
                            <td>{{$video_data->video_title}}</td>
                            <td>{{$video_data->video_url}}</td>
                            <td>{{$video_data->video_notes}}</td>
                            <td>{{$video_data->web_page_url}}</td>
                            <td>
                              <div class="btn-group action-btn">
                                <a href="{{route('admin.videomanager.edit',$video_data->id)}}" class="btn btn-flat btn-default">
                                    <i data-toggle="tooltip" data-placement="top" title="" class="fa fa-pencil" data-original-title="Edit"></i>
                                </a>
                                                 
                                <a class="btn btn-flat btn-default" data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure you want to do this?" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
                            
                                <form action="{{route('admin.videomanager.delete',$video_data->id)}}" method="POST" name="delete_item" style="display:none">
                                    {{csrf_field()}}
                                   <input type="hidden" name="_method" value="delete">
                                </form>
                                </a>
                             </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('CMSpage') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        $(function() {
            var dataTable = $('#videomanager_table').dataTable({
                 
            });

        });
    </script>
@endsection