@extends ('backend.layouts.app') 

@section ('title', "LandingPage")

@section('page-header')
<h1>
    LandingPage
    <small>{{ trans('labels.backend.settings.edit') }}</small>
</h1>
@endsection 

@section('content') 
{{ Form::model($landingpage, ['route' => ['admin.landingpage.update'], 'class' => 'form-horizontal',
'role' => 'form', 'method' => 'POST','files' => true, 'id' => 'edit-landingpage']) }}

<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title">{{ trans('labels.backend.settings.edit') }}</h3>
    </div>
    <div class="box-body setting-block">
        <!-- Nav tabs -->
        <ul id="myTab" class="nav nav-tabs setting-tab-list" role="tablist">
            <li role="presentation" class="active">
                <a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Section1</a>
            </li>
            <li role="presentation">
                <a href="#tab2" aria-controls="1" role="tab" data-toggle="tab">Section2</a>
            </li>
            <li role="presentation">
                <a href="#tab3" aria-controls="2" role="tab" data-toggle="tab">Section3</a>
            </li>
            <li role="presentation">
                <a href="#tab4" aria-controls="3" role="tab" data-toggle="tab">Section4</a>
            </li>
            <li role="presentation">
                <a href="#tab5" aria-controls="4" role="tab" data-toggle="tab">Footer</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div id="myTabContent" class="tab-content setting-tab">
            <div role="tabpanel" class="tab-pane active" id="tab1">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tip</label>
                    <div class="col-lg-8">
                        <input type="text" name="tip1" class="form-control" value="{{$landingpage->tip1}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <textarea type="text" name="title1" class="form-control" placeholder="Title" rows="5">{{$landingpage->title1}}</textarea>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea type="text" name="explain1" class="form-control" placeholder="Title" rows="5">{{$landingpage->explain1}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('image1', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('image1', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="image1">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        
                    </div>
                    <!--col-lg-10-->
                </div>

                <div class="form-group">
                    {{ Form::label('video1', 'Video', ['class' => 'col-lg-2 control-label file_upload']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('video1', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="video1">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                    </div>
                    <!--col-lg-10-->
                </div>
                <!--form control-->
            </div>
            <div role="tabpanel" class="tab-pane" id="tab2">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tip</label>
                    <div class="col-lg-8">
                        <input type="text" name="tip2" class="form-control" value="{{$landingpage->tip2}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <textarea type="text" name="title2" class="form-control" placeholder="Title" rows="5">{{$landingpage->title2}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="explain2" class="form-control" placeholder="Explain" rows="5">
                            {{$landingpage->explain2}}
                        </textarea>
                    </div>
                </div>
                 <div class="form-group">
                    {{ Form::label('image2', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('image2', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="image2">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                        <div class="img-remove-logo hidden">
                            <img height="50" width="50" src="">
                            <i id="remove-logo-img" class="fa fa-times remove-logo" data-id="logo" aria-hidden="true"></i>
                        </div>
                    </div>
                    <!--col-lg-10-->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tip</label>
                    <div class="col-lg-8">
                        <input type="text" name="tip3" class="form-control" value="{{$landingpage->tip3}}" placeholder="Tip" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <textarea type="text" name="title3" class="form-control" placeholder="Title" rows="5">{{$landingpage->title3}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="explain3" class="form-control" placeholder="Explain" rows="5">
                            {{$landingpage->explain3}}
                        </textarea>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('video3', 'Video', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">

                        <div class="custom-file-input">
                            {!! Form::file('video3', array('class'=>'form-control inputfile inputfile-1' )) !!}
                            <label for="video3">
                                <i class="fa fa-upload"></i>
                                <span>Choose a file</span>
                            </label>
                        </div>
                    </div>
                    <!--col-lg-10-->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab4">
                 <div class="form-group">
                    <label class="col-lg-2 control-label">Title</label>
                    <div class="col-lg-8">
                        <input type="text" name="title4" class="form-control" value="{{$landingpage->title4}}" placeholder="Title" row="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Explain</label>
                    <div class="col-lg-8">
                        <textarea name="explain4" class="form-control" placeholder="Explain" rows="5">
                            {{$landingpage->explain4}}
                        </textarea>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab5">
                 <div class="form-group">
                    <label class="col-lg-2 control-label">Footer</label>
                    <div class="col-lg-8">
                        <input type="text" name="footer" class="form-control" value="{{$landingpage->footer}}" placeholder="Footer" row="2">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10 footer-btn">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div><!--box-->

<!-- hidden setting id variable -->
<input type="hidden" value="{{ $landingpage->id }}" id="landingpage" name="landingpage">
<input type="hidden" data-id="{{ $landingpage->id }}" id="setting">

{{ Form::close() }} 
@endsection 

@section('after-scripts')
<script src='/js/backend/bootstrap-tabcollapse.js'></script>
<!-- <script src="{{asset('js/backend/uploads.js')}}"></script> -->
<script>
    (function(){
        Backend.Utils.csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        Backend.Settings.selectors.RouteURL = "{{ route('admin.removeIcon', -1) }}";
        Backend.Settings.init();
        
    })();

    window.load = function(){
        
    }
    
    $('#myTab').tabCollapse({
        tabsClass: 'hidden-sm hidden-xs',
        accordionClass: 'visible-sm visible-xs'
    });
    $('.inputfile').bind('change', function() {

      //this.files[0].size gets the size of your file.
      if(this.files[0].size > 4194304)
      {

        alert("Filesize have to be small that 4 Megabytes");

      }

    });
</script>
@endsection