@extends('frontend.layouts.app')

@section('after-styles')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/terms.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
	<div class="container card-deck-body">
	  <div class="text-center col-lg-12">
	  	<span class="header_title">TERMS AND CONDITIONS</span>
	  </div> 
	</div>
</div>
<hr>
<div class="container">
<div class="card mb-4 box-shadow col-lg-12">
	          
  <div class="card-body">
    <ul class="list-unstyled mb-4">
			{{settings()->terms}}
    </ul>
  </div>
</div>
</div>
@endsection