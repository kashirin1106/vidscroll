@extends('frontend.layouts.app')

@section('after-styles')
	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/dashboard.css')}}">
@endsection

@section('content')
<?php $membership = \App\Models\Membership\UserMembership::get_membership()?>
<?php $subscribe = \App\Models\Membership\UserMembership::get_subscribe()?>
    <div class="card-deck">
        <div class="card-deck-body text-center">
            <span class="header_title">PROFILE</span>
        </div>
    </div>
       
 <div class="container dashboard">
    <div class="col-lg-6">
        <img id="logo" src="{{asset('img/frontend/logo/1.png')}}">
    </div>
    <!-- /////////////////////////////////////////////// -->
    <div class="col-lg-6 main">
        <div class="details">
            <span><h2>My current Membership</h2></span>
        </div>
        <div class="details">
            <span class="pull-right">
            @if(!empty($subscribe))    
            @if($subscribe->gateway == 0)
                <a type="button" class="btn btn-primary" href="{{url('/subscription/str_cancel')}}">
            @else       
                <a type="button" class="btn btn-primary" href="{{route('frontend.paypal.cancel',$subscribe->stripe_id)}}">
            @endif      
                Cancel</a></li>
             @endif   
            </span>
        </div>
        <div class="details">
        <label>
            @if(!empty($membership))
            <h4>{{$membership->name}} with @if($subscribe->gateway == 1) PAYPAL @else STRIPE @endif : {{$membership->price}}{{$membership->currency}}</h4>
            @endif
        </label>     
        </div>
            @if(!empty($membership))
         <div class="form-group">
        {{ Form::label('enable_save_title', "Enable Save Title", ['class' => 'col-lg-10 control-label']) }}

            <div class="col-lg-2">
                <div class="control-group">
                    <label class="control control--checkbox">
                        {{ Form::checkbox('enable_save_title', 1, ($membership->enable_save_title == 1) ? true : false ) }}
                        <div class="control__indicator"></div>
                    </label>
                </div>
            </div><!--col-lg-3-->
        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('enable_save_notes', "Enable Save Note", ['class' => 'col-lg-10 control-label']) }}

            <div class="col-lg-2">
                <div class="control-group">
                    <label class="control control--checkbox">
                        {{ Form::checkbox('enable_save_notes', 1, ($membership->enable_save_notes == 1) ? true : false ) }}
                        <div class="control__indicator"></div>
                    </label>
                </div>
            </div><!--col-lg-3-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('enable_video_counts', "Enable Video Count", ['class' => 'col-lg-9 control-label required']) }}

            <div class="col-lg-3">
                <input type="text" name="enable_video_counts" class ="form-control box-size" value="{{$membership->enable_video_counts}}" readonly>
            </div><!--col-lg-10-->
        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('enable_screenshot_counts', "Enable Screenshot Count", ['class' => 'col-lg-9 control-label required']) }}

            <div class="col-lg-3">
                <input type="text" name="enable_screenshot_counts" class ="form-control box-size" value="{{$membership->enable_screenshot_counts}}" readonly>
            </div><!--col-lg-10-->
        </div><!--form control-->
        <div class="col-lg-12">
            <label><h4>Membership Start Date : {{$subscribe->created_at}}</h4></label>
        </div>
        <div class="col-lg-12">
            <label><h4>Membership End Date  : {{$subscribe->ends_at}}</h4></label>
        </div>
        @endif
         <div class="col-lg-12">
            <span><h4>Current Downloaded Videos : {{ $current_videos }}</h4></span>
            <span><h4>Current Saved Screenshots :  {{ $current_screens }}</h4></span>
        </div>
    </div>
</div>
@endsection
 
@section('after-scripts')
    <script>
       $(":checkbox").on('click',function(){
        return false;
       })
    </script>
@endsection