@extends('frontend.layouts.app')

@section('after-styles')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/membership.css')}}">
@endsection

@section('content')
<div class="card-deck mt-8">
  <div class="container card-deck-body">
    <div class="text-center col-lg-12">
      <span class="header_title">MEMBERSHIP</span>
    </div> 
  </div>
</div>
<hr>
<section id="introduce">
	<div class="container">
		<div class="col-lg-12">
			<div class="col-lg-6">
         <form method="post" class="" action="{{route('frontend.paypal.start')}}">
  				<span class="col-lg-12"><h3>Membership : {{$membership->name}}</h3></span>
          <input type="text" class="d-none hidden" name="budget" value="{{$membership->price}}">
          <input type="text" class="d-none hidden" name="membership_id" value="{{$membership->id}}">
          <input type="text" class="d-none hidden" name="type" value="{{$membership->type}}">
  				<span class="col-lg-12" id="note">{{$membershippage->title1}}</span>
  				<span class="col-lg-12"><h3>{{$membershippage->explain1}}</h3></span>
  				<span class="col-lg-12" id="comment"></span>
  				<!-- <button class="btn btn-lg btn-success">START WITH PAYPAL</button> -->

          <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#paypal_modal">START WITH PAYPAL</button>
          <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">START WITH STRIPE</button>
        </form>
			</div>
			<div class="col-lg-6">
				<img style="width: 100%" src="{{$membershippage->image1}}">
			</div>
		</div>
	</div>
</section>
   
<section id="remember" class="text-center">
	<div class="col-lg-12">
		<span class="col-lg-12" id="everything">{{$membershippage->title2}}</span>
		<span class="col-lg-12">
			<h3>{{$membershippage->explain2}}</h3>
		</span>
	</div>		
	<div class="container card-deck-body">
	  <div class="text-center col-lg-12 option_card">
	  	
        <div class="card mb-4 box-shadow col-lg-4">
          <div class="card-header">
            <img src="{{$membershippage->icon1}}">
            <h3><span>{{$membershippage->icon_title1}}</span></h3>
            <hr>
          </div>
          <div class="card-body">
            <span><h4>
              	{{$membershippage->icon_explain1}}
              </h4>
            </span>   
          </div>
        </div>

        <div class="card mb-4 box-shadow col-lg-4">
          <div class="card-header">
            <img src="{{$membershippage->icon2}}">
            <h3><span>{{$membershippage->icon_title2}}</span></h3>
            <hr>
          </div>
          <div class="card-body">
            <span><h4>
              	{{$membershippage->icon_explain2}}
              </h4>
            </span>   
          </div>
        </div>

        <div class="card mb-4 box-shadow col-lg-4">
          <div class="card-header">
            <img src="{{$membershippage->icon3}}">
            <h3><span>{{$membershippage->icon_title3}}</span></h3>
            <hr>
          </div>
          <div class="card-body">
            <span><h4>
              	{{$membershippage->icon_explain3}}
              </h4>
            </span>   
          </div>
        </div>
 
	  </div> 
	</div>
</section>
<section id="assistant" class="hidden">
	<div class="col-lg-12">
		<div class="col-lg-6">
			<span>Use Google or Siri as your digital assistant?</span>
			<span>They speak Evernote too. Create, update, or search your note by voice command.</span>
			<div class="buttons">
				<div class="cta ">
					<a class="button button-app-store apple" href="https://itunes.apple.com/us/app/evernote/id281796108?mt=8">
						Download on the App Store
					</a>
				</div>
				<div class="cta ">
					<a class="button button-app-store google" href="https://play.google.com/store/apps/details?id=com.evernote">
						Get it on Google Play
					</a>
					
				</div>
		 	</div>
		</div>
		<div class="col-lg-6">
			
		</div>
	</div>
</section>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Stripe Subscription</h4>
        </div>
        <form action="{{ url('subscription') }}" method="post" id="payment-form">
          @csrf  
        <div class="modal-body">
          <div class="card">
                                 
                    <div class="form-group">
                        <div class="card-header">
                            <label for="card-element">
                                Enter your credit card information
                            </label>
                        </div>
                        <div class="card-body">
                            <div id="card-element">
                            <!-- A Stripe Element will be inserted here. -->
                            </div>
                            <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                             <label for="card-element">
                                Do you have coupon code?
                            </label>
                            <input type="text" class="form-control" name="coupon_code">
                            <input type="hidden" name="plan" value="{{ $membership->id }}" />
                        </div>
                    </div>
                   
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-primary" type="submit">Continue</button>
        </div>
       </form>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="paypal_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Paypal Subscription</h4>
        </div>
 
        <form method="post" id="payment-form" class="" action="{{route('frontend.paypal.start')}}">
          @csrf  
        <div class="modal-body">
          <div class="card">
                    <div class="form-group">
                      <!-- <span class="col-lg-12"><h3>Membership : {{$membership->name}}</h3></span> -->
                      <input type="text" class="d-none hidden" name="budget" value="{{$membership->price}}">
                      <input type="text" class="d-none hidden" name="membership_id" value="{{$membership->id}}">
                      <input type="text" class="d-none hidden" name="type" value="{{$membership->type}}">
                      <!-- <span class="col-lg-12" id="note">{{$membershippage->title1}}</span> -->
                      <!-- <span class="col-lg-12"><h3>{{$membershippage->explain1}}</h3></span> -->
                    </div>
                    <div class="form-group">
                        <div class="card-header">
                            <label for="card-element">
                                Do you have coupon code?
                            </label>
                            <input type="text" class="form-control" name="coupon_code">
                        </div>
                        <div class="card-body">
                            <div id="card-element">
                            
                            </div>
                        </div>
                    </div>
                   
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-primary" type="submit">Continue</button>
        </div>
       </form>
      </div>
      
    </div>
  </div>
@endsection
@section('after-scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
    // Create a Stripe client.
var stripe = Stripe('{{ env("STRIPE_KEY") }}');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {slrgn
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});
//////////////////////to becomws as 
// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);
console.log(hiddenInput);
  // Submit the form
  form.submit();
}
</script>
@endsection