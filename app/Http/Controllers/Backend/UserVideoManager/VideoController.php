<?php

namespace App\Http\Controllers\Backend\UserVideoManager;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\UserMembership;
use App\Models\Membership\MembershipPackage;
use App\Models\Access\User\User;
use App\Models\UserVideoManager\Video;
/**
 * Class MembershipController.
 */
class VideoController extends Controller
{
    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        $user_membership = UserMembership::all();
        $video = Video::all();
        return view('backend.uservmanager.index')->with('video',$video);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create()
    {
        $memberships = MembershipPackage::all();
        $all_users   = User::all();
        return view('backend.uservmanager.create')->with('memberships',$memberships)->with('all_users',$all_users);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function stores(Request $request)
    {
        $new_user_video = new Video;

        $new_user_video->user_id        = $request->user_id;
        $new_user_video->video_title    = $request->video_title;
        $new_user_video->video_url      = $request->video_url;
        $new_user_video->video_notes    = $request->video_notes;
        $new_user_video->web_page_url   = $request->web_page_url;
        
        $new_user_video->save();
        return redirect()->back();
    }

    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit(Request $request, $id)
    {
        $user_membership = UserMembership::find($id);
        $user_video      = Video::find($id);
        return view('backend.uservmanager.edit')->with('user_membership',$user_membership)->with('user_video',$user_video);
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $user_video = Video::find($id);
        $user_video->user_id      = $request->user_id;
        $user_video->video_title  = $request->video_title;
        $user_video->video_notes  = $request->video_notes;
        $user_video->video_url    = $request->video_url;
        $user_video->web_page_url = $request->web_page_url;

        $user_video->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function delete($id)
    {
        $user_video = Video::find($id);
        if($user_video->delete()){
            return new RedirectResponse(route('admin.videomanager'), ['flash_success' => "Successfully deleted!"]);
        }
    }
}
