<?php

namespace App\Http\Controllers\Backend\Membershippage;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\UserMembership;
use App\Models\Membershippage\Membershippage;
/**
 * Class MembershipController.
 */
class MembershippageController extends Controller
{
    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit()
    {
        $membershippage = Membershippage::first();
        
        return view('backend.membershippage.edit')->with('membershippage',$membershippage);
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request)
    {
        $membershippage = Membershippage::find($request->membershippage);

        $membershippage->title1 = $request->title1;
        $membershippage->explain1 = $request->explain1;
        
        if($image1 = $request->file('image1'))
        {
            $file_name =time().'.'.$image1->getClientOriginalExtension();
            $image1_url = ('/membershippage/image/').$file_name;
            $image1->move(public_path('/membershippage/image'),$image1_url);
            $membershippage->image1 = $image1_url;
        }
        
        $membershippage->title2 = $request->title2;
        $membershippage->explain2 = $request->explain2;

        if($icon1 = $request->file('icon1'))
        {
            $file_name = time().'.'.$icon1->getClientOriginalExtension();
            $icon1_url = ('/membershippage/image/').$file_name;
            $icon1->move(public_path('membershippage/image'),$icon1_url);
            $membershippage->icon1 = $icon1_url;
        }
        $membershippage->icon_title1 = $request->icon_title1;
        $membershippage->icon_explain1 = $request->icon_explain1;

        if($icon2 = $request->file('icon2'))
        {
            $file_name = time().'.'.$icon2->getClientOriginalExtension();
            $icon2_url = ('/membershippage/image/').$file_name;
            $icon2->move(public_path('membershippage/image'),$icon2_url);
            $membershippage->icon2 = $icon2_url;
        }
        $membershippage->icon_title2 = $request->icon_title2;
        $membershippage->icon_explain2 = $request->icon_explain2;

        if($icon3 = $request->file('icon3'))
        {
            $file_name = time().'.'.$icon3->getClientOriginalExtension();
            $icon3_url = ('/membershippage/image/').$file_name;
            $icon3->move(public_path('membershippage/image'),$icon3_url);
            $membershippage->icon3 = $icon3_url;
        }
        $membershippage->icon_title3 = $request->icon_title1;
        $membershippage->icon_explain3 = $request->icon_explain1;

        $membershippage->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }
 
}
