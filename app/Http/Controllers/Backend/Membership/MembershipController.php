<?php

namespace App\Http\Controllers\Backend\Membership;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\MembershipPackage;

/**
 * Class MembershipController.
 */
class MembershipController extends Controller
{
    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        $membership_packages = MembershipPackage::all();
        return view('backend.membership.index')->with('membership_packages',$membership_packages);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create()
    {
        return view('backend.membership.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function stores(Request $request)
    {
        $new_membership_package = new MembershipPackage;

        $new_membership_package->name  = $request->title;
        $new_membership_package->price = $request->price;
        $new_membership_package->type  = $request->type;
        $new_membership_package->enable_save_title = $request->enable_save_title;
        $new_membership_package->enable_save_notes = $request->enable_save_notes;
        $new_membership_package->enable_video_counts = $request->enable_video_counts;
        $new_membership_package->enable_screenshot_counts = $request->enable_screenshot_counts;

        $new_membership_package->save();
        return redirect()->back();
    }

    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit(Request $request, $id)
    {
        $membership = MembershipPackage::find($id);
        return view('backend.membership.edit')->with('membership',$membership)->with('flash_success','Successfully updated');
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $membership_packages = MembershipPackage::find($id);
        $membership_packages->name = $request->title;
        $membership_packages->price = $request->price;
        $membership_packages->type = $request->type;
        $membership_packages->enable_save_title = $request->enable_save_title;
        $membership_packages->enable_save_notes = $request->enable_save_notes;
        $membership_packages->enable_video_counts = $request->enable_video_counts;
        $membership_packages->enable_screenshot_counts = $request->enable_screenshot_counts;

        $membership_packages->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function delete($id)
    {
        $membership_packages = MembershipPackage::find($id);
        if($membership_packages->delete()){
            return new RedirectResponse(route('admin.membership'), ['flash_success' => "Successfully deleted!"]);
        }
    }
}
