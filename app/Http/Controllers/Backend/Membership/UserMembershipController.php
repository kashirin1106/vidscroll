<?php

namespace App\Http\Controllers\Backend\Membership;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\UserMembership;
use App\Models\Membership\MembershipPackage;
use App\Models\Access\User\User;
/**
 * Class MembershipController.
 */
class UserMembershipController extends Controller
{
    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        $user_membership = UserMembership::all();
        
        return view('backend.user_membership.index')->with('user_membership',$user_membership);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create()
    {
        $memberships = MembershipPackage::all();
        $all_users   = User::all();
        return view('backend.user_membership.create')->with('memberships',$memberships)->with('all_users',$all_users);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function stores(Request $request)
    {
        $new_membership_package = new UserMembership;

        $new_membership_package->user_id = $request->user_id;
        $new_membership_package->stripe_plan = $request->membership_id;
        $new_membership_package->created_at  = $request->start_at;
        $new_membership_package->ends_at    = $request->end_at;
        
        $new_membership_package->save();
        return redirect()->back();
    }

    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit(Request $request, $id)
    {
        $user_membership = UserMembership::find($id);
        $memberships     = MembershipPackage::all();
        return view('backend.user_membership.edit')->with('user_membership',$user_membership)->with('memberships',$memberships);
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $membership_packages = UserMembership::find($id);
        $membership_packages->user_id = $request->user_id;
        $membership_packages->stripe_plan = $request->membership_id;
        $membership_packages->created_at = $request->start_at;
        $membership_packages->ends_at = $request->end_at;

        $membership_packages->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function delete($id)
    {
        $user_membership = UserMembership::find($id);
        if($user_membership->delete()){
            return new RedirectResponse(route('admin.user_membership'), ['flash_success' => "Successfully deleted!"]);
        }
    }
}
