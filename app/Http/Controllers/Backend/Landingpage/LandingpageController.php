<?php

namespace App\Http\Controllers\Backend\Landingpage;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page\Page;
use App\Repositories\Backend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\UserMembership;
use App\Models\Landingpage\Landingpage;
/**
 * Class MembershipController.
 */
class LandingpageController extends Controller
{
    /**
     * @param \App\Models\Page\Page                            $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Page\EditResponse
     */
    public function edit()
    {
        $landingpage = Landingpage::first();
        
        return view('backend.landingpage.edit')->with('landingpage',$landingpage);
    }

    /**
     * @param \App\Models\Page\Page                              $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Request $request)
    {
        $landingpage = Landingpage::find($request->landingpage);
        $landingpage->tip1 = $request->tip1;
        $landingpage->title1 = $request->title1;
        $landingpage->explain1 = $request->explain1;
        
        if($image1 = $request->file('image1'))
        {
            $file_name =time().'.'.$image1->getClientOriginalExtension();
            $image1_url = ('/landingpage/image/').$file_name;
            $image1->move(public_path('/landingpage/image'),$image1_url);
            $landingpage->image1 = $image1_url;
        }

        if($video1 = $request->file('video1'))
        {
            $file_name =time().'.'.$video1->getClientOriginalExtension();
            $video1_url = ('/landingpage/video/').$file_name;
            $video1->move(public_path('/landingpage/video'),$video1_url);
            $landingpage->video1 = $video1_url;
        }
        /*
            section 2
        */
        $landingpage->tip2 = $request->tip2;
        $landingpage->title2 = $request->title2;
        $landingpage->explain2 = $request->explain2;

        if($image2 = $request->file('image2'))
        {
            $file_name =time().'.'.$image2->getClientOriginalExtension();
            $image2_url = ('/landingpage/image/').$file_name;
            $image2->move(public_path('/landingpage/image'),$image2_url);
            $landingpage->image2 = $image2_url;
        }
        /*
            section 3
        */
        $landingpage->tip3 = $request->tip3;
        $landingpage->title3 = $request->title3;
        $landingpage->explain3 = $request->explain3;

         if($video3 = $request->file('video3'))
        {
            $file_name =time().'.'.$video3->getClientOriginalExtension();
            $video3_url = ('/landingpage/video/').$file_name;
            $video3->move(public_path('/landingpage/video'),$video3_url);
            $landingpage->video3 = $video1_url;
        }
        /*
            section 4
        */
        $landingpage->title4 = $request->title4;
        $landingpage->explain4 = $request->explain4;

        $landingpage->footer  = $request->footer;

        $landingpage->save();

        return redirect()->back()->with('flash_success',"Successfully updated!");
    }
 
}
