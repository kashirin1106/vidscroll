<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Membership\MembershipPackage;
use App\Models\Feature\Screenshots;
use App\Models\UserVideoManager\Video;
use Illuminate\Http\Request;

/**
 * Class FeatureController.
 */
class FeatureController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $video = Video::where('user_id',$user_id)->get();
        
        return view('frontend.feature.index', compact('video'));
    }
    
    public function silver()
    {
        $membership = MembershipPackage::where('id','3')->first();
        return view('frontend.membership.membership',compact('membership'));
    }

    public function edit($id)
    {
        $video_detail = Video::where('id',$id)->first();
        return view('frontend.feature.edit',compact('video_detail'));
    }

    public function screenshot(Request $request)
    {
        $user_id = auth()->user()->id;
        $video_url = $request->query('id');
        
        $video_detail = Video::where('video_url',$video_url)->first();
        $screenshot = Screenshots::where('video_link',$video_url)->where('user_id',$user_id)->get();
        return view('frontend.feature.screenshot',compact('screenshot','video_detail'));
    }

    public function update(Request $request, $id)
    {
        $update_video = Video::where('id',$id)->first();

        $update_video->video_title = $request->video_title;
        $update_video->video_notes = $request->video_notes;
        if($update_video->save())
        {
            return redirect()->back()->withFlashSuccess('Video has been Updated');
        }
        else{
            return redirect()->back()->withFlashDanger('Video update failue');
        }
    }
    public function delete($id)
    {
        $delete_video = Video::where('id',$id)->first();
        if($delete_video->delete())
        {
            return redirect()->back()->withFlashSuccess('Your Video has been deleted!');
        }else{
            return reidrect()->back()->withFlashDanger('Server error');
        }
    }
}
