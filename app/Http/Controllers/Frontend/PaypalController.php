<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

// use to process billing agreements
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use App\Models\Membership\Subscriptions;
use App\Models\Coupon\Coupon;

class PaypalController extends Controller
{
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;
    private $plan_id;
    public  $type;
    public  $membership_id;
    public  $agreementId;
    
    // Create a new instance with our paypal credentials
    public function __construct()
    {
         $paypal_conf = \Config::get('paypal');
        
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'] 
            )
        );
        $this->apiContext->setConfig($paypal_conf['settings']);
        // Set the Paypal API Context/Credentials
    }

    public function create_plan(Request $request){
        /* check if he is registered now */
        if($request->user()->subscribed('main')) {
            return redirect()->back()->withFlashSuccess('You have already subscribed the plan');
        }
        $createdAgreement = Subscriptions::where('user_id',auth()->user()->id)->where('gateway',1)->value('stripe_id');
         if(!is_null($createdAgreement)){
            try {
                $agreement = Agreement::get($createdAgreement, $this->apiContext);
                if($agreement->state == "Active")
                {
                    return redirect()->back()->withFlashSuccess('You have already subscribed the plan');
                }
            } catch (Exception $ex) {
                 dd($agreement->getId(), $createdAgreement->getId(), $ex);
                
                exit(1);
            }
         }
        /* end */
        $validatedData = $request->validate([
            'budget' => 'required',
            'membership_id' => 'required',
            'type' => 'required',
        ]);
        
        // dd($request);

        $interval = 14;
        $type = $request->type;
        $type_type = "REGULAR";
         
         if($type == "YEAR") $interval = 1;

        $budget = $request->budget;

        if($request->coupon_code != ''){
            $coupon_data = Coupon::where('coupon_code',$request->coupon_code)->first();

            if($coupon_data != null){
                session()->put('coupon_id', $coupon_data->id);
                $coupon_date = $coupon_data->period;
                $coupon_discount = $coupon_data->discount;
                if($coupon_date > 0){
                    $interval += $coupon_date;
                    if($interval > 366) $interval = 365;
                }
                if($coupon_discount > 0){
                    $budget -= $coupon_discount;
                    
                    if($budget < 0)
                        return redirect()->back()->withFlashDanger("There was something wrong with your coupon code!");
                }
            }
        }
        $membership_id = $request->membership_id;
        session()->put('membership_id', $membership_id);
        // Create a new billing plan
        $plan = new Plan();
        $plan->setName('App Name Monthly Billing')
          ->setDescription('Monthly Subscription to the App Name')
          ->setType('infinite');
        
        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
          ->setType($type_type)
          ->setFrequency($type)
          ->setFrequencyInterval(1)
          ->setCycles('0')
          ->setAmount(new Currency(array('value' => $budget, 'currency' => 'USD')));

        /*$chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
        ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));

        $paymentDefinition->setChargeModels(array($chargeModel));  */
    // dd($interval);
        $interval = 9;
        $paymentFreeDefinition = new PaymentDefinition();
        $paymentFreeDefinition->setName('Regular Payments')
          ->setType("TRIAL")
          ->setFrequency("DAY")
          ->setFrequencyInterval($interval)
          ->setCycles('1')
          ->setAmount(new Currency(array('value' =>'0', 'currency' => 'USD')));
          
          //here is the monthly amount
        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl('http://vidscroll.vidscrollapp.com/subscribe/paypal/return')
          ->setCancelUrl('http://vidscroll.vidscrollapp.com/subscribe/paypal/return')
          ->setAutoBillAmount('yes')
          ->setInitialFailAmountAction('CONTINUE')
          ->setMaxFailAttempts('0');

        $plan->setPaymentDefinitions(array($paymentFreeDefinition,$paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        //create the plan
        try {
            // dd($plan);
            $createdPlan = $plan->create($this->apiContext);
            try {
                $patch = new Patch();
                $value = new PayPalModel('{"state":"ACTIVE"}');
                $patch->setOp('replace')
                  ->setPath('/')
                  ->setValue($value);
                $patchRequest = new PatchRequest();
                $patchRequest->addPatch($patch);
                $createdPlan->update($patchRequest, $this->apiContext);
                $plan = Plan::get($createdPlan->getId(), $this->apiContext);

                // Output plan id
                // echo 'Plan ID:' . $plan->getId();
                $this->plan_id = $plan->getId();
                
                $agreement = new Agreement();
                $agreement->setName('App Name Monthly Subscription Agreement')
                  ->setDescription('Basic Subscription')
                  ->setStartDate(\Carbon\Carbon::now()->addMinutes(5)->toIso8601String());

                // Set plan id
                $plan = new Plan();
                $plan->setId($this->plan_id);
                $agreement->setPlan($plan);

                // Add payer type
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');
                $agreement->setPayer($payer);

                try {
                  // Create agreement
                  $agreement = $agreement->create($this->apiContext);
                  $agreementId = $agreement->getId();
                  $this->agreementId = $agreementId;
                  // Extract approval URL to redirect user
                  $approvalUrl = $agreement->getApprovalLink();

                  return redirect($approvalUrl);
                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                  echo $ex->getCode();
                  echo $ex->getData();
                  die($ex);
                } catch (Exception $ex) {
                  die($ex);
                }

            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                die($ex);
            }
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }

    }
 
    public function paypalReturn(Request $request){

        $token = $request->token;
        $agreement = new \PayPal\Api\Agreement();

        try {

            // Execute agreement
            $result = $agreement->execute($token, $this->apiContext);
           
            $agreementId = $result->getId();
            $user = auth()->user();
            $user->is_stripe = 0;
            if(isset($result->id)){
                $user->stripe_id  = $result->id;
            }
            $user->save();

            $ends_at = date('Y-m-d H:i:s', strtotime('+1 month'));
            $userid = auth()->user()->id;
            
            $subscription = new Subscriptions;

            $subscription->gateway = 1;
            $subscription->user_id = $userid;
            $subscription->name = "main";
            $subscription->stripe_id = $agreementId;
            $subscription->stripe_plan = session()->get('membership_id');
            if(session()->get('coupon_id'))
            $subscription->coupon_id = session()->get('coupon_id');
            $subscription->ends_at = $ends_at;
             
            $subscription->save();
            return redirect()->route('frontend.user.dashboard')->withFlashSuccess('You are subscribed successfully!');

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return redirect()->route('frontend.user.account')->withFlashDanger('You have either cancelled the request or your session has expired!');
        }
    }

    public function cancel($agreementId)
    {
        $agreement = new \PayPal\Api\Agreement();

        $agreement->setId($agreementId);
        $agreementStateDescriptor = new \PayPal\Api\AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Cancel the agreement");

        try {
            $agreement->cancel($agreementStateDescriptor, $this->apiContext);
            $cancelAgreementDetails = \PayPal\Api\Agreement::get($agreement->getId(), $this->apiContext);   
            $status = Subscriptions::where('stripe_id',$agreementId)->first();
            $status->status = "canceled";
            $status->save();

            return redirect()->route('frontend.user.account')->withFlashDanger("Membership has been canceled!");

        } catch (Exception $ex) {    

            return redirect()->back()->withFlashDanger("Server error!");
            
        }
    }
}
