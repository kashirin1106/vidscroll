<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Models\Membership\MembershipPackage as Plan;
use App\Models\Access\User\User;
use Carbon\Carbon;
use App\Models\Membership\Subscriptions;
use PayPal\Api\Agreement;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class StripeController extends Controller
{
     public function __construct()
    {
         $paypal_conf = \Config::get('paypal');
        
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'] 
            )
        );
        $this->apiContext->setConfig($paypal_conf['settings']);
        // Set the Paypal API Context/Credentials
    }
    
    public function create(Request $request, Plan $plan)
    {
        $createdAgreement = Subscriptions::where('user_id',auth()->user()->id)->where('gateway',1)->value('stripe_id');
        
        if(!is_null($createdAgreement)){
            $agreement = Agreement::get($createdAgreement, $this->apiContext);
            if($agreement->state == "Active")
            {
                return redirect()->back()->withFlashSuccess("You are already subscribed the plan");
            }
        }    
       
        if($request->user()->subscribed('main')) {
             return redirect()->back()->withFlashSuccess('You have already subscribed the plan');
        }
        
         $plan = Plan::findOrFail($request->get('plan'));
        Plan::create(array(
          "price" => 2000,
          "interval" => "month",
          "name" => "Amazing Gold Plan",
          "currency" => "usd",
          "id" => "gold")
        );
        $request->user()->createAsStripeCustomer();

        if(!is_null($request->coupon_code)){
            
            $request->user()
                ->newSubscription('main', $plan->stripe_plan)
                ->withCoupon($request->coupon_code)
                ->create($request->stripeToken);
             
        }else{
 
              $request->user()
                ->newSubscription('main', $plan->stripe_plan)
                ->create($request->stripeToken);
            }
         return redirect()->route('frontend.user.dashboard')->withFlashSuccess('Your plan has been subscribed');
    }

    public function plan()
    {
        dd("ok");
    }
 
  public function cancel()
    {
        $id   = auth()->user()->id;
        $user = User::findOrFail($id);
        if($user->subscription('main')->cancelNow())
        {
            $status = Subscriptions::where('user_id',$id)->first();
            $status->status = "canceled";
            $status->save();
        }

        return redirect()->route('frontend.user.account')->withFlashDanger('Your plan has been canceled');
    }

     public function resume()
    {
        $id   = auth()->user()->id;
        $user = User::findOrFail($id);

        if ($user->subscription('main')->onGracePeriod()) {
            $user->subscription('main')->resume();
        }
        
        return redirect()->route('frontend.user.account')->withFlashDanger('Your plan has been resumed');
    }
    public function upgrade($plan)
    {
        try {
            Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $user = User::find(1);

            $user->subscription('main')->swap($plan);

            return 'Plan changed successfully!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }    
}