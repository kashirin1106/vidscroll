<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Access\User\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\UserVideoManager\Video;
use Image;
use App\Models\Feature\Screenshots;

class VideoSaveController extends APIController
{
    /**
     * Log the user in.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function video_capture_data_1(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_email'     => 'required|email',
            'image_data'     => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }
        $user_id = User::where('email',$request->user_email)->value('id');
        $permission = User::getPermission($user_id);
        $count_screenshots = Screenshots::where('user_id',$user_id)->count();
        if($count_screenshots > $permission->enable_screenshot_counts)
            return $this->respond([
                'message'     => "You can not save any more screenshots!",
                'status_code' => $this->getStatusCode(),
            ]);
        $new_image_url = "origin-".time().str_random(10).".png";
        $path = public_path('img/screenshot/prototype/'.$new_image_url);
        Image::make(file_get_contents($request->image_data))->resize(200,200)->save($path);     

        $thumbnail_path = public_path('img/screenshot/thumbnail/'.$new_image_url);
        $thumbnailImage = Image::make($request->image_data);
        $thumbnailImage->resize(50,50);
        $thumbnailImage->save($thumbnail_path);

        $videos = new Screenshots;
        $videos->user_id      = $user_id;
        $videos->timeframe    = $request->timeframe;
        // $videos->index        = $request->index;
        $videos->web_page_url = $request->web_url_value;
        $videos->video_link   = $request->video_link;
        if(strpos($request->video_link,"youtube") !== false){
            $videos->video_link   = $request->web_url_value;
        }
        $video_note = $request->note;
        if(is_null($video_note)) $video_note = ' ';
        $videos->note         = $video_note;
        $videos->screenshot_url=$new_image_url;

        try {
            if ($videos->save()) {
                return $this->respond([
                    'message'   => "Saved Successfully!",
                    'code'      => "ok"
                ]);
            }
        } catch (JWTException $e) {
            return $this->respondInternalError($e->getMessage());
        }

    }
 
    public function video_capture_main(Request $request)
    {
         $validation = Validator::make($request->all(), [
            'user_email'    => 'required|email',
            'video_url'     => 'required',
            'web_url'       => 'required',
            'category'      => 'required'
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }
        $user_id = User::where('email',$request->user_email)->value('id');
        $permission = User::getPermission($user_id);
        $count_videos = Video::where('user_id',$user_id)->count();

         if($count_videos > $permission->enable_video_counts)
            return $this->respond([
                'error' => [
                    'message'     => "You can not save any more videos!",
                    'status_code' => $this->getStatusCode(),
                ],
            ]);

        $check_exist = Video::where('web_page_url', $request->web_url)->where('video_url',$request->video_url)->first();
        if(strpos($request->video_url,"youtube") !== false){
            $check_exist = Video::where('slug',$request->video_url)->where('web_page_url', $request->web_url)->first();
        }    
        if($check_exist == '')
            $new_main_data = new Video;
        else
            $new_main_data = $check_exist;
      /*  if($request->is_exist == 1)
            $new_main_data = Video::where('web_page_url', $request->web_url)->where('video_url',$request->video_url)->first();*/
        $new_main_data->user_id     = User::where('email',$request->user_email)->value('id');
        $new_main_data->video_title = $request->video_title;
        if($permission->enable_save_title != 1)
            $new_main_data->video_title = '';

        $new_main_data->video_notes = $request->note;
        if($permission->enable_save_notes != 1)
            $new_main_data->video_notes = '';

        $new_main_data->video_url   = $request->video_url;
        $new_main_data->web_page_url= $request->web_url;
        if(strpos($request->video_url,"youtube") !== false){
            $new_main_data->video_url   = $request->web_url;
            $new_main_data->slug        = $request->video_url;
        }
        $new_main_data->category    = $request->category;

        if($new_main_data->save()){
            return $this->respond([
                'message'  => "Successfully saved!",
                'status_code' => $this->getStatusCode(),
            ]);
        }else{
             return $this->respond([
                'message'     => "There is something wrong!",
                'status_code' => $this->getStatusCode(),
            ]);
        }
        
    }

    public function video_category(Request $request)
    {
        $user_id = User::where('email',$request->user_email)->value('id');
        $categories = Video::where('user_id',$user_id)->pluck('category');
        $unique = $categories->unique();
        $ret = $unique->values()->all();
        // return json_decode(json_encode($unique), true);
        /*  unset($property->rooms[$key]);
        $property->rooms->values();*/
        return $ret;
    }
    public function video_title(Request $request)
    {
        $categories = Video::pluck('video_title');
        $unique = $categories->unique();
        $ret = $unique->values()->all();
         
        // return json_decode(json_encode($unique), true);
        /*  unset($property->rooms[$key]);
        $property->rooms->values();*/
        return $ret;
    }
    public function update_results(Request $request){
        $ret = $request->all();
        $is_save = 0;
        for ($i=0; $i < count($ret); $i++) { 
            $id = $ret[$i]["id"];
            $update_screen = Screenshots::find($id);
            $update_screen->note = $ret[$i]["value"];
            if($update_screen->save()){
            $is_save = 1;
            }else{
                 return $this->respond([
                     'error'  =>  [
                            'message'       =>  "There is something wrong!",
                            'status_code'   =>  $this->getStatusCode(),
                     ]
                ]);
            }
        }

            return $this->respond([
                'message'     =>  "Successfully saved!",
                'status_code' =>  $this->getStatusCode(),
            ]);
             
    }

     public function delete_results(Request $request){
        $ret = $request->all();
        
        $is_save = 0;
        for ($i=0; $i < count($ret); $i++) { 
            $id = $ret[$i]["id"];
            
            $delete_screen = Screenshots::find($id);
            
            if($delete_screen->delete()){
            $is_del = 1;
            }else{
                 return $this->respond([
                     'error'  =>  [
                            'message'       =>  "There is something wrong!",
                            'status_code'   =>  $this->getStatusCode(),
                     ]
                ]);
            }
        }

            return $this->respond([
                'message'     =>  "Successfully deleted!",
                'status_code' =>  $this->getStatusCode(),
                'deleted'     =>  $ret
            ]);
             
    }

    public function delete_video_results(Request $request){
            $ret = $request->video_id;
        
            $delete_video = Video::find($ret);
            $video_url = $delete_video->video_url;
            if($delete_video->delete()){
                $delete_screens = Screenshots::where('video_link',$video_url)->delete();

               return $this->respond([
                    'message'       =>  "Successfully deleted!",
                    'status_code'   =>  $this->getStatusCode(),
                    'deleted_video' => $ret
                ]);
            }else{
                 return $this->respond([
                     'error'  =>  [
                            'message'       =>  "There is something wrong!",
                            'status_code'   =>  $this->getStatusCode(),
                     ]
                ]);
            }
    }
            
    public function delete_cate_results(Request $request){
        $ret = $request->delete_results;

        $user_email = $request->user_email;

        $user_id = User::where('email',$user_email)->value('id');
        $is_del = 0;
        for ($i=0; $i < count($ret); $i++) { 
            $id = $ret[$i]["id"];
            $delete_cate = Video::where('user_id',$user_id)->where('category',$id)->pluck('id');
            
            for($j=0;$j<count($delete_cate);$j++){
                $delete_category[$j] = Video::find($delete_cate[$j]); 
                $delete_category[$j]->category = null; 
                 if($delete_category[$j]->save()){
                 $is_del = 1;
                }
             
                if($is_del == 0) {
                     return $this->respond([
                         'error'  =>  [
                                'message'       =>  "There is something wrong!",
                                'status_code'   =>  $this->getStatusCode(),
                         ]
                    ]);
                }
            }
        }

            return $this->respond([
                'message'     =>  "Successfully deleted!",
                'status_code' =>  $this->getStatusCode(),
                'delete_cate' =>  $ret
            ]);
    }

    public function check_db(Request $request)
    {
        $user_id = User::where('email',$request->user_email)->value('id');
        $check_db = Video::where('user_id',$user_id)->where('video_url',$request->video_url)->where('web_page_url',$request->web_url)->first();
        
        if($request->web_url == $request->video_url){
            $check_db = Video::where('user_id',$user_id)->where('video_url',$request->video_url)->first();
        } else if(strpos($request->web_url,"youtube") !== false){
             $check_db = Video::where('user_id',$user_id)->where('slug',$request->video_url)->where('web_page_url',$request->web_url)->first();
        }
        
        if($check_db == '')
            return $this->respond([
                'result'     =>  ""
            ]);
        
        return $this->respond([
            'result'     =>  $check_db
        ]);
    }

    public function detail_video_update(Request $request)
    {
        $video_update = Video::find($request->video_id);
        $video_update->video_title = $request->video_title;
        $video_update->category    = $request->video_category;
        $video_update->video_notes = $request->video_note;

        if($video_update->save()){
            return $this->respond([
                'message'     =>  "Successfully Updated!",
                'status_code' =>  $this->getStatusCode(),
            ]);
        }else{
            return $this->respond([
                     'error'  =>  [
                            'message'       =>  "There is something wrong!",
                            'status_code'   =>  $this->getStatusCode(),
                     ]
                ]);
        }
    }
}
