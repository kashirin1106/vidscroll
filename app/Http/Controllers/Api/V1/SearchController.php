<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Access\User\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\UserVideoManager\Video;
use App\Models\Feature\Screenshots;
use Illuminate\Support\Facades\DB;
use App\Notifications\Frontend\Auth\EmailSharing;

class SearchController extends APIController
{
    /**
     * Log the user in.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'search_content'     => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }
        // $search_result =  Video::where('video_title', 'LIKE', '%'.$request->search_content_name.'%')->get();
        $user_email = $request->user_email;
        $user_id    = User::where('email',$user_email)->value('id');

        $search_video =  Video::where('id', $request->search_content)->where('user_id',$user_id)->first();

        if(!$search_video){
            return $this->respond([
                'code'   => 204,
                'search_video'  => $search_video,
                'message'       => "There is not same video!"
            ]);
        }
        $search_screen = Screenshots::where('video_link', $search_video->video_url)->where('user_id',$user_id)->get();
        if(strpos($request->web_url_value,"youtube") !== false){
                $search_screen = Screenshots::where('video_link', $search_video->video_url)->where('user_id',$user_id)->get();
         }   
            return $this->respond([
                'code'   => 200,
                'search_video'  => $search_video,
                'search_screen' => $search_screen
            ]);
    }

 public function search_upper(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'search_content_name'     => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }
        $user_email = $request->user_email;
        $user_id    = User::where('email',$user_email)->value('id');

        if($request->option == "name")
            $search_result = Video::where('video_title','LIKE','%'.$request->search_content_name.'%')->where('user_id',$user_id)->get();
        else
            $search_result = Video::where('category', $request->search_content_name)->where('user_id',$user_id)->get();

        if(!$search_result->isNotEmpty()){
            return $this->respond([
                'error' =>[
                'message' => 'No such Video!'
                ]
            ]);
        }    
        return $this->respond([
            'search_result' => $search_result,
            'message'       => "succss",
            'code'          =>  200
        ]);
    }

    public function share_data(Request $request)
    {
        $user_id     = User::where('email',$request->user_email)->value('id');
        $video_id    = $request->video_id;
        $share_email = $request->share_email;
        
        // $share_phone = $request->share_phone
        /*if email exist*/
        if($share_email != '')
        {
            $user = new User;
            $user->email = $share_email;
            
            $user->notify(new EmailSharing($video_id, $request->user_email));
             return $this->respond([
                'message'       => "Sharing successed!",
                'code'          =>  200
            ]);
        
        }
        return $this->respond([
                'error' =>[
                    'message' => 'There is something wrong with sharing!'
                ]
            ]);
        /*if phone exist*/
       /* if($share_phone != '')
        {

        }*/
    }
}
