<?php

namespace App\Models\Feature;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Page\Traits\Attribute\PageAttribute;
use App\Models\Page\Traits\PageRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
use App\Models\UserVideoManager\Video;

class Screenshots extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
        PageRelationship,
        PageAttribute {
            // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */

    protected $with = ['owner'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "screenshots";
    }
    public function get_video_name($id)
    {
        $video_name = Video::where('id',$id)->value('video_title');
        return $video_name;
    }
}
